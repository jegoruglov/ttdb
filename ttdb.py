from bson.objectid import ObjectId
from bson import json_util
import json
import os
import time

import gridfs
import pymongo

import tornado.ioloop
import tornado.web
import tornado.autoreload
import tornado.httpserver
import tornado.options

import conf

settings = {
    'debug': True
}

def custom_id(f):
    def wrapper(self, collection):
        last_key = self.last_key(collection)
        document = json.loads(self.request.body)
        if not document.has_key('_id'):
            document['_id'] = last_key
            self.request.body = json.dumps(document)
        response = f(self, collection)
        if response == last_key:
            _response = self.increment_last_key(collection)
    return wrapper


def auth(f):
    def wrapper(self, *args, **kwargs):
        if (settings['debug'] or 
                self.get_argument('key', None) in conf.authorized_api_keys):
            f(self, *args, **kwargs)
        else:
            self.finish({
                'status': -1,
                'data': None,
                'msg': 'Unauthorized'
            })
    return wrapper

class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.client = pymongo.MongoClient('localhost', 27017)
        self.db = self.client['warehouse']
        self.fs = gridfs.GridFS(self.db)
        #self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", 
            "Origin, X-Requested-With, Content-Type, Accept")
        self.set_header("Access-Control-Allow-Methods", "GET, POST, DELETE, OPTIONS")

    def write(self, data):
        response = json.dumps(data, default=json_util.default)
        super(BaseHandler, self).write(response)

    def write_binary(self,data):
        super(BaseHandler, self).write(data)        

    def object_id(self, _id):
        try:
            return ObjectId(_id)
        except:
            pass
        try:
            return int(_id)
        except:
            pass
        return _id

    def last_key(self, collection):
        return self.db.keys.find_one({'_id': collection})['last_key']

    def increment_last_key(self, collection):
        doc = self.db.keys.find_one({'_id': collection})
        doc['last_key'] += 1
        return self.db.keys.save(doc)

class RESTHandler(BaseHandler):
    @auth
    def get(self, collection, _id=None):
        if _id is None:
            if (self.request.arguments == {} or 
                    self.request.arguments.keys() == ['key']):
                response = list(self.db[collection].find())
                self.write(response)
                return
            else:
                try:
                    self.request.arguments.pop('key')
                except:
                    pass
                args = dict([(k, v[0]) for k, v 
                    in self.request.arguments.iteritems()])
                self.write(list(self.db[collection].find(args)))
                return
        else:
            object_id = self.object_id(_id)
            self.write(self.db[collection].find_one(
                {'_id': object_id}))
            return

    @auth
    @custom_id
    def post(self, collection):
        document = json.loads(self.request.body)
        _id = self.db[collection].save(document)
        document['_id'] = _id
        self.write(document)
        return _id

    @auth
    def delete(self, collection, _id):
        object_id = self.object_id(_id)
        response = self.db[collection].remove({'_id': object_id})
        self.write(response)

    @auth
    def options(self, *args, **kwargs):
        self.write('')

class ImagesHandler(BaseHandler):
    @auth
    def get(self, _id=None):
        if _id is None:
            data = [str(i['_id']) for i in list(self.db.fs.files.find())]
            self.write(data)
        else:
            try:
                fd = self.fs.get(self.object_id(_id))
            except gridfs.NoFile:
                self.write('no file')
                return
            data = fd.read()
            self.add_header('Content-Type','image/jpeg')
            self.write_binary(data)

    @auth
    def post(self, _id):
        data = json.loads(self.request.body)['data'].encode('latin1')
        response = self.fs.put(data)
        self.write({'image': str(response)})

    @auth
    def delete(self, _id):
        response = self.fs.delete(self.object_id(_id))
        self.write(response)

    @auth
    def options(self, *args, **kwargs):
        self.write('')

class CustomHandler(BaseHandler):
    @auth
    def get(self, query):
        if query == 'categories':
            categories = self.db.products.distinct('category')
            self.write(categories)
            return

class GetCollections(BaseHandler):
    @auth
    def get(self):
        schemas = self.db['schemas']
        collections = list(schemas.find())
        collections = [dict(name=i['name'], id=i['id']) for i in collections]
        self.write(collections)

class GetAllHandler(BaseHandler):
    @auth
    def get(self, collection_name):
        collection = self.db[collection_name]
        data = list(collection.find())
        self.write(data)

class GetWarehouseHandler(BaseHandler):
    @auth
    def get(self):
        result = []
        schemas = self.db['schemas']
        collections = list(schemas.find())
        collections = [i['id'] for i in collections]
        for collection_name in collections:
            collection = self.db[collection_name]
            collection = list(collection.find())
            for i in collection:
                i['product_type'] = collection_name
            result.extend(collection)
        self.write(result)

class GetOneHandler(BaseHandler):
    @auth
    def get(self, collection_name, doc_id):
        collection = self.db[collection_name]
        data = collection.find_one(ObjectId(doc_id))
        self.write(data)

class GetSchemaHandler(BaseHandler):
    @auth
    def get(self, collection_id):
        schemas = self.db.schemas
        data = schemas.find_one({'id': collection_id})
        data['id'] = data['id']
        data['name'] = data['name']
        data['fields'] = [i.capitalize() for i in data['fields']]
        self.write(data)

class InsertHandler(BaseHandler):
    @auth
    def post(self):
        data = json.loads(self.request.body)
        collection_name = data['id']
        collection = self.db[collection_name]
        t = int(time.time())
        d = dict([(i['id'], i['new_value']) for i in data['fields']])
        d['added_time'] = t
        obj = collection.insert(d)
        d['_id'] = obj
        self.write(d)

class UpdateHandler(BaseHandler):
    @auth
    def post(self):
        data = json.loads(self.request.body)
        collection_name = data['collection']
        collection = self.db[collection_name]
        doc = collection.find_one(ObjectId(data['_id']))
        doc = data['data']
        doc['_id'] = ObjectId(data['_id'])
        obj = collection.save(doc)
        self.write(obj)

class DeleteHandler(BaseHandler):
    @auth
    def post(self):
        data = json.loads(self.request.body)
        collection_name = data['collection']
        collection = self.db[collection_name]
        obj = collection.remove(ObjectId(data['_id']))
        self.write(obj)

class ArchiveHandler(BaseHandler):
    @auth
    def post(self):
        data = json.loads(self.request.body)
        collection_name = data['collection']
        collection = self.db[collection_name]
        doc = collection.find_one(ObjectId(data['_id']))
        doc['archived'] = True
        obj = collection.save(doc)
        self.write(obj)

class DearchiveHandler(BaseHandler):
    @auth
    def post(self):
        data = json.loads(self.request.body)
        collection_name = data['collection']
        collection = self.db[collection_name]
        doc = collection.find_one(ObjectId(data['_id']))
        doc['archived'] = False
        obj = collection.save(doc)
        self.write(obj)

class CreateCollection(BaseHandler):
    @auth
    def get(self, collection_name):
        collection_name = u'X_{}'.format(collection_name)
        try:
            result = self.db.create_collection(collection_name)
            # create_colleciton returns object which is not JSON convertible
            result = ''
            error = ''
        except pymongo.errors.CollectionInvalid as e:
            result = ''
            error = str(e)
        self.write({
            'status': error and -1 or 0,
            'data': result,
            'msg': error
        })

class DeleteCollection(BaseHandler):
    @auth
    def get(self, collection_name):
        collection_name = u'X_{}'.format(collection_name)
        result = self.db.drop_collection(collection_name)
        self.write('')
    
class CreateOrderHandler(BaseHandler):
    @auth
    def post(self):
        order = json.loads(self.request.body)
        cart = order['cart']
        customer = order['customer']
        billing_info = customer['billing_info']
        shipping_info = customer['shipping_info']
        #TODO: validation
        email = billing_info['email']
        uniq_domain = email.split('@')[1]
        account = self.db.accounts.find_one({'uniq_domain': uniq_domain})
        returning_account = account and True or False
        if not account:
            account = self.db.accounts.save({'uniq_domain': uniq_domain})
            account_id = str(account)
        else:
            account_id = str(account['_id'])
        customer = self.db.customers.find_one({'email': email})
        returning_customer = customer and True or False
        if not customer:
            customer = billing_info
            customer['account_id'] = account_id
            customer = self.db.customers.save(customer)
            customer_id = str(customer)
        else:
            customer_id = str(customer['_id'])
        order['account_id'] = account_id
        order['returning_account'] = returning_account
        order['customer_id'] = customer_id
        order['returning_customer'] = returning_customer
        new_id = int(self.db.counters.find_and_modify(
            query={'_id': 'orderid'},
            update={'$inc':{'seq': 1}},
            upsert=True)['seq'])
        order['order_id'] = new_id
        order = self.db.orders.save(order)
        self.write({
            'status': 0,
            'data': str(order),
            'msg': ''
        })

class OrderHandler(BaseHandler):
    @auth
    def get(self, order_id):
        order = self.db.orders.find_one(ObjectId(order_id))
        self.write({
            'status': 0,
            'data': order,
            'msg': ''
        })

application = tornado.web.Application([
    (r"/(categories)/?", CustomHandler),
    (r"/images/([^/]+)", ImagesHandler),
    (r"/images/?", ImagesHandler),
    (r"/([^/]+)/?", RESTHandler),
    (r"/([^/]+)/([^/]+)/?", RESTHandler),
    (r"/get/([^/]*)/?", GetAllHandler),
    (r"/get/([^/]*)/([^/]*)/?", GetOneHandler),
    (r"/insert/?", InsertHandler),
    (r"/update/?", UpdateHandler),
    (r"/delete/?", DeleteHandler),
    (r"/remove/([^/]*)/([^/]*)/?", DeleteHandler),
    (r"/get-collections/?", GetCollections),
    (r"/create-collection/([^/]*)/?", CreateCollection),
    (r"/delete-collection/([^/]*)/?", DeleteCollection),
    (r"/archive/?", ArchiveHandler),
    (r"/dearchive/?", DearchiveHandler),
    (r"/schema/([^/]*)/?", GetSchemaHandler),
    (r"/warehouse/?", GetWarehouseHandler),
    (r"/checkout/?", CreateOrderHandler),
    (r"/order/([^/]*)/?", OrderHandler)
], **settings)

if __name__ == "__main__":
    tornado.options.define("port", default=9818, help="run on the given port", type=int)
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(tornado.options.options.port)
    tornado.autoreload.start()
    tornado.ioloop.IOLoop.instance().start()
